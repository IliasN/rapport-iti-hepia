# Remerciements {-} 

Vos remerciements ici

# Résumé {-}

Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé  Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé Texte de votre résumé

Quelques informations sont à changer ci-dessous. Elles sont changeables dans `0-preface.md`.

\begin{figure}
  \vspace{.1cm}
  \begin{center}
    \includegraphics[width=3.72cm,height=2.4cm]{figs/front-logo.png}
  \end{center}
\end{figure}
\begin{tabular}{ p{3cm} p{1cm} p{1cm} p{6cm}  }
  \multicolumn{1}{l}{Candidat :}& & & \multicolumn{1}{l}{Professeur-e(s) responsable(s) :}\\
  \multicolumn{1}{l}{\textbf{John Doe}} & & & \multicolumn{1}{l}{\textbf{Paul Dupont}} \\
  \multicolumn{1}{l}{Filière d'études : ITI} & & & \multicolumn{1}{l}{Travail de semestre soumis à une convention de stage en} \\
  \multicolumn{1}{l}{} & & & \multicolumn{1}{l}{entreprise : non} \\
  \multicolumn{1}{l}{} & & & \multicolumn{1}{l}{Travail soumis à un contrat de confidentialité : non} \\
\end{tabular}


\pagebreak