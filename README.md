# Structure de rapport HEPIA - HES-SO

Ce repo vous permet de créer rapidement un rapport de semestre ou de bachelor conformément aux demandes de l'HEPIA.

## Installation

Il faut installer [Pandoc](https://pandoc.org/). Pandoc 2.9 c'est mieux. Il faut l'installer avec un `.deb` car les versions sur les repos sont en général tous nuls et tout vieux.

Il faut ensuite [XeLaTeX](https://packages.debian.org/fr/sid/texlive-xetex). Il permet de convertir notre rapport de LaTeX en `.pdf`.

Il y a probablement d'autres paquets à installer, mais pas de panique : en lançant la compilation de votre rapport, Pandoc vous indiquera si quelque chose manque, ce en plantant lamentablement. C'est très drôle à voir.

## Je n'ai pas envie d'installer tout plein de trucs sur mon ordi !

Pas de souci. En pushant sur votre repo git (je vous recommande de faire un git juste pour votre rapport), GitLab va automatiquement générer votre rapport.
Une fois pushé, votre rapport, s'il est écrit correctement, possèdera une petite coche verte. En cliquant sur _download_, vous pouvez télécharger un artefact nommé _build_ qui contient votre rapport.
Un conseil cependant : je vous recommande d'installer pandoc sur votre ordinateur, vous perdrez moins de temps à attendre que GitLab compile votre rapport. Ca peut prendre du temps !

## Structure de dossiers et fichiers

- `figs` : Vos figures, c'est-à-dire vos images du rapport. Vous pouvez faire des sous-dossiers si vous voulez. Voir plus bas pour comment mettre une image facilement dans votre rapport.
- `templates` : La partie LaTeX du rapport. Touchez pas trop sauf si nécessaire.
- `text` : Votre rapport en markdown. Je décris plus bas comment ça marche.
- `config.yaml` : C'est là que vous pourrez configurer la plupart des informations de votre rapport. Pensez à changer les infos là dedans.
- `figs/front-logo.png` : Le logo utilisé pour votre rapport. Changez cette image. 

Les autres fichiers ne sont pas spécialement importants, ne les supprimez pas pour autant.

## Compilation de votre rapport

Ce qui est tout à fait classe avec Pandoc, c'est que vous pouvez écrire votre rapport en Markdown et il vous le convertira en PDF proprement. Vous pouvez même mettre du LaTeX dedans, cela fonctionnera convenablement. Il y a toutefois quelques indications à suivre (voir plus bas).

Avant tout, pensez à changer les informations dans `config.yaml`. Elles changeront automatiquement dans tout le travail. Par exemple, si vous changez la valeur `sensei` dans `config.yaml`, le nom de votre professeur responsable sera changé dans tout le rapport.
Il faut aussi changer quelques valeurs dans `0-preface.md`, dans le LaTeX.

Pour lancer la compilation de votre rapport, faites simplement `make`. Pour nettoyer votre projet, faites `make clean`. Pas trop compliqué en somme.

## Les trucs qui sont faits pour vous

- Le titre
- La table des matières
- La table des figures
- Le glossaire
- La bibliographie

## Les trucs PAS faits pour vous

Cette section est plus importante que celle du dessus.

- La table des annexes
- La vérification de la syntaxe et de l'orthographe
- Le texte : le contenu, les remerciements, le résumé, l'introduction et la conclusion.

## Indications d'usage

Dans le désordre, quelques infos à savoir sur ce template.

- Mettez vos fichier markdown dans le dossier `text`.

- Mettez vos image dans le dossier `figs`.

- Vous **DEVEZ** mettre vos fichiers markdown dans l'ordre alphabétique d'apparition. Par exemple, si vous avez une préface, une introduction, un chapitre, et une conclusion, vous nommerez vos fichiers :

  - `1-préface.md`
  - `2-introduction.md`
  - `3-chapitre-un.md`
  - `4-conclusion.md`

  Le nom après le chiffre (et le tiret) n'est pas important, mais le reste l'est. 

- Il **faut** finir tous ces fichiers Markdown dans `text` par un `\pagebreak`. Cela permet de séparer chaque document par un saut de page. Ce n'est pas nécessaire d'en mettre un pour le dernier fichier markdown (cela ferait une page vierge à la fin du document)

- Il **faut** avoir un fichier de bibliographie en format BibLaTeX nommé `my.bib`. Vous pouvez référencer votre bibliographie en faisant [@le_nom_de_votre_entrée_dans_votre_bibliographie]. Si vous n'avez pas de fichier de ce type, créez-le juste vide. Zotero a une extension [Better BibTex](https://retorque.re/zotero-better-bibtex/) qui permet de synchroniser votre Zotero directement dans my.bib, c'est super pratique, je vous recommande de l'utiliser.

- Je vous fournis du LaTeX dans certains fichiers markdown. Il n'est pas là par hasard. N'y touchez pas, sans quoi vous risquez de tout casser. 

- Vous pouvez ajouter des notes de bas de page, des listes, des tableaux, tout plein de trucs en bouquinant [la documentation Pandoc](https://pandoc.org/MANUAL.html). Je vous recommande d'y jeter un œil, c'est fort sympa ce qu'on peut faire sans trop s'embêter.

- Il y a une URL dans `text/ZZ-glossaire.md`. **Lisez la doc**.

- Si vous faites des tableaux Markdown dans votre rapport, et que vous voulez qu'il apparaisse dans la liste des tables, ajouter `Table: <la légende de votre tableau>` juste après votre tableau, à la ligne d'après. Sans ligne vide entre votre tableau et votre légende.

- Je vous déconseille de mettre du code dans votre rapport. Ma template déconne pas mal avec. Si vous DEVEZ en mettre, contentez-vous de petits extraits ou de mettre des screens.

- Si le texte semi-transparent en haut à gauche de chaque slide est décalé, vous pouvez faire un réglage dans le fichier `templates/default.tex` à la ligne 250.

- J'ai été flemmard. Si une entreprise vous mandate pour votre travail de Bachelor/semestre, il faudra ajouter cela manuellement (probablement dans `0-preface.md`).

## Raccourcis

Je vous ai simplifiés la vie au plus : il y a des raccourcis pour aller plus vite. La syntaxe est un peu dégueu, mais c'est pas la mer à boire.

Rappel : une figure = une image.

```latex
% Insère une image (et l'ajoute dans la table des figures) :
\img{Le chemin vers l'image}{les options de l'image, comme la taille (scale=0.2)}{La légende de l'image}{La source de l'image}
\cimg fait la même chose, mais au centre.

Exemple : \cimg{figs/internet-minute.jpg}{scale=0.5}{Ce que produit Internet chaque minute (en 2019)}{Source : tiré de \url{https://www.visualcapitalist.com/what-happens-in-an-internet-minute-in-2019/}, ref. URL01}
```

## Remerciements

Merci au Docteur Orestis Malaspinas pour sa sagesse de l'utilisation de Pandoc et de la promotion tout à fait positive qu'il en a faite.

Merci à Monsieur Heirich d'avoir été mon cobaye pour la création de ce template.